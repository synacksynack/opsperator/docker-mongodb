SKIP_SQUASH?=1
IMAGE=opsperator/mongodb
FRONTNAME=opsperator
-include Makefile.cust

.PHONY: build
build:
	@@SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: test
test:
	@@docker rm -f testmongodb || true
	@@docker run \
	    -e MONGODB_ADMIN_PASSWORD=secret \
	    -e MONGODB_DATABASE=testdb \
	    -e MONGODB_USER=myusr \
	    -e MONGODB_PASSWORD=testpw \
	    --name testmongodb \
	    -d $(IMAGE)

.PHONY: testrs
testrs:
	@@docker rm -f testmongodb || true
	@@docker run \
	    --name testmongodb \
	    --add-host=mongodb-0:127.0.0.1 \
	    --entrypoint=container-entrypoint \
	    --hostname=mongodb-0 \
	    -e CI_MOCK=yay \
	    -e DEBUG=yay \
	    -e HOSTNAME=mongodb-0 \
	    -e MONGODB_REPLICA_NAME=thisismymgreplicaname \
	    -e MONGODB_USER=mguser \
	    -e MONGODB_KEYFILE_VALUE=thisismymgkeyfilevalue \
	    -e MONGODB_EXPOSE_LOCAL=yay \
	    -e MONGODB_PASSWORD=mgpassword \
	    -e MONGODB_SERVICE_NAME=mongodb-0 \
	    -e MONGODB_DATABASE=mgdb \
	    -e MONGODB_ADMIN_PASSWORD=thisismymgadminpw \
	    -p 27017:27017 \
	    -d $(IMAGE) \
	    /usr/bin/run-mongod-replication

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in secret service statefulset; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "MONGODB_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "MONGODB_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-ha.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
	@@oc process -f deploy/openshift/secret.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true

.PHONY: ocdemoephemeral
ocdemoephemeral: ocbuild
	@@if ! oc describe secret mongodb-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemopersistent
ocdemopersistent: ocbuild
	@@if ! oc describe secret mongodb-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemo
ocdemo: ocdemoephemeral

.PHONY: ocprod
ocprod: ocbuild
	@@if ! oc describe secret mongodb-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret-prod.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-ha.yaml -p FRONTNAME=$(FRONTNAME) \
	    | oc apply -f-

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true
