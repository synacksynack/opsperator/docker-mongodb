#!/bin/sh

if test "`id -u`" -ne 0; then
    if test -s /tmp/mongodb-passwd; then
	echo Skipping nsswrapper setup - already initialized
    else
	echo Setting up nsswrapper mapping `id -u` to mongodb
	sed "s|^mongodb:.*|mongodb:x:`id -g`:|" /etc/group >/tmp/mongodb-group
	sed \
	    "s|^mongodb:.*|mongodb:x:`id -u`:`id -g`:mongodb:/var/lib/mongodb:/usr/sbin/nologin|" \
	    /etc/passwd >/tmp/mongodb-passwd
    fi
    export NSS_WRAPPER_PASSWD=/tmp/mongodb-passwd
    export NSS_WRAPPER_GROUP=/tmp/mongodb-group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi
