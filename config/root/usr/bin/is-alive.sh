#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

mongo 127.0.0.1:27017/$MONGODB_DATABASE \
    -u $MONGODB_USER -p $MONGODB_PASSWORD \
    --eval="quit();" >/dev/null 2>&1

exit $?
