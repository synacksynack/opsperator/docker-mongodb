#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

if ! /usr/bin/is-alive.sh; then
    echo Mongo is unavailable >&2
    exit 1
fi

mongo 127.0.0.1:27017/$MONGODB_DATABASE \
    -u $MONGODB_USER -p $MONGODB_PASSWORD \
    --eval="rs.isMaster();" 2>/dev/null \
    | grep -E '"ismaster"[ \t]*:[ \t]*true,' >/dev/null

exit $?
