#!/bin/bash

set -o errexit
set -o pipefail

if test "$DEBUG"; then
    set -x
fi
source "${CONTAINER_SCRIPTS_PATH}/common.sh"

readonly MEMBER_HOST="$(hostname -f)"

function initiate() {
    local host="$1"
    local config="{_id: '${MONGODB_REPLICA_NAME}', members: [{_id: 0, host: '${host}'}]}"

    info "Initiating MongoDB replica using: ${config}"
    mongo_cmd --host 127.0.0.1 --quiet <<<"quit(rs.initiate(${config}).ok ? 0 : 1)"
    info "Waiting for PRIMARY status ..."
    mongo_cmd --host 127.0.0.1 --quiet <<<"while (!rs.isMaster().ismaster) { sleep(100); }"
    info "Successfully initialized replica set"
}

function add_member() {
    local host="$1"
    info "Adding ${host} to replica set ..."

    if ! mongo_cmd "$(replset_addr admin)" -u admin -p"${MONGODB_ADMIN_PASSWORD}" --quiet <<<"while (!rs.add('${host}').ok) { sleep(100); }"; then
	info "ERROR: couldn't add host to replica set!"
	return 1
    fi
    info "Waiting for PRIMARY/SECONDARY status ..."
    mongo_cmd --host 127.0.0.1 --quiet <<<"while (!rs.isMaster().ismaster && !rs.isMaster().secondary) { sleep(100); }"
    info "Successfully joined replica set"
}


info "Waiting for local MongoDB to accept connections  ..."
wait_for_mongo_up
if [[ $(mongo_cmd --host 127.0.0.1 --quiet <<<'db.isMaster().setName') == "${MONGODB_REPLICA_NAME}" ]]; then
    info "Replica set '${MONGODB_REPLICA_NAME}' already exists, skipping initialization"
    >/tmp/initialized
    exit 0
fi

if test "$MEMBER_ID" = 0; then
    initiate "${MEMBER_HOST}"
else
    add_member "${MEMBER_HOST}"
fi
