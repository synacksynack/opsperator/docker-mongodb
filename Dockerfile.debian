FROM debian:buster-slim

# MongoDB server image for OpenShift Origin

ENV CONTAINER_SCRIPTS_PATH=/usr/share/container-scripts/mongodb \
    DEBIAN_FRONTEND=noninteractive \
    HOME=/var/lib/mongodb \
    MONGODB_VERSION=4.4 \
    VERSION=4.4.10

LABEL io.k8s.description="MongoDB database server" \
      io.k8s.display-name="MongoDB $VERSION" \
      io.openshift.expose-services="27017:mongodb" \
      io.openshift.non-scalable="false" \
      io.openshift.tags="database,mongodb" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-mongodb" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$VERSION"

COPY config/root /

RUN set -x \
    && echo "# Install Dumb-init" \
    && apt-get update \
    && apt-get install --no-install-recommends --no-install-suggests -y \
	dumb-init gnupg wget libnss-wrapper gettext dnsutils groff-base \
	python-minimal ca-certificates \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Installing MongoDB" \
    && ln -sf /bin/true /bin/systemctl \
    && wget -qO - \
	https://www.mongodb.org/static/pgp/server-${MONGODB_VERSION}.asc \
	| apt-key add - \
    && echo \
	"deb http://repo.mongodb.org/apt/debian buster/mongodb-org/${MONGODB_VERSION} main" \
	>/etc/apt/sources.list.d/mongodb-org.list \
    && apt-get update \
    && apt-get -y install mongodb-org \
    && usermod -a -G root mongodb \
    && echo "# Fixing Permissions" \
    && for dir in $HOME $HOME/data; \
	do \
	    mkdir -p $dir 2>/dev/null \
	    && chown -R 1001:root "$dir" \
	    && chmod -R g=u "$dir"; \
	done \
    && >/etc/mongod.conf \
    && chown 1001:root /etc/mongod.conf \
	$CONTAINER_SCRIPTS_PATH/mongod.conf.template \
    && chmod 664 /etc/mongod.conf \
	$CONTAINER_SCRIPTS_PATH/mongod.conf.template \
    && echo "# Cleaning Up" \
    && apt-get -y remove --purge gnupg wget ca-certificates \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rvf /usr/share/man /usr/share/doc /var/lib/apt/lists/* \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
ENTRYPOINT ["dumb-init","--","container-entrypoint"]
CMD ["run-mongod"]
