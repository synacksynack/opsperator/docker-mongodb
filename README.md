# k8s Mongo

Forked from https://github.com/sclorg/mongodb-container

Build with:

```
$ make build
```

Start Demo or Cluster in OpenShift:

```
$ make ocdemo
$ make ocprod
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name           |    Description                | Default                                     |
| :------------------------- | ----------------------------- | ------------------------------------------- |
|  `MONGODB_ADMIN_PASSWORD`  | MongoDB Admin Password        | undef / mandatory                           |
|  `MONGODB_DATABASE`        | MongoDB Database Name         | `secret`                                    |
|  `MONGODB_EXPOSE_LOCAL`    | MongoDB Exposes Local to App  | undef / optional                            |
|  `MONGODB_KEYFILE_VALUE`   | MongoDB KeyFile Value         | undef / optional, mandatory running replset |
|  `MONGODB_PASSWORD`        | MongoDB App User Password     | undef / mandatory                           |
|  `MONGODB_REPLICA_NAME`    | MongoDB ReplicaSet Name       | undef / optional, mandatory running replset |
|  `MONGODB_SERVICE_NAME`    | MongoDB Internal Service Name | undef / optional, mandatory running replset |
|  `MONGODB_USER`            | MongoDB App Username          | undef / mandatory                           |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point        | Description              |
| :------------------------- | ------------------------ |
|  `/var/lib/mongodb/data`   | MongoDB Database Assets  |
